# OptiBoost

A program that changes Windows settings, registry, and GPU to maximize your PC performance. (ONLY TESTED ON WINDOWS 10 & 11)

## FAQ

### Do I need to leave it open at all times?

No, once you're happy with your settings, you may close it and even delete the program from your PC.

### Can't download / PC thinks it's a virus

Make sure to disable Windows Virus & Threat Protection (Real-Time Protection). If you don't know how, feel free to use this link: https://bit.ly/LyodsTweak-WinRTP.

If the first method doesn't work, add an exclusion for OptiBoost on your Anti-Virus. You can find a list of Anti-Virus that find OptiBoost safe & unsafe at https://bit.ly/LyodsTweak-AVRGS.

If you don't feel safe without Windows default Anti-Virus on, use a better 3rd-party program like Malwarebytes. (You can re-enable it after setting the tweak.)

## Credits

- MansHere
- Lyod

## Disclaimer

This application collects your desktop username to count unique users and is sent to a private Discord channel (Example only). No other data is sent.